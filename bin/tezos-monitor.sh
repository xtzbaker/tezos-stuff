#!/bin/bash


export WORKING_DIR=/home/tezos/tezos-monitor

. ${WORKING_DIR}/private.config

export OUTPUT=${WORKING_DIR}/missed.txt
export NOTIFICATIONS_FILE=${WORKING_DIR}/notifications.txt
export PROTOCOLS_FILE=${WORKING_DIR}/protocols.txt

export BAKER=${PRIVATE_BAKER_ADDRESS}
export ZAPIER_URL=${PRIVATE_ZAPIER_URL}

CURRENT_PROTOCOL=$(curl -s https://api6.tzscan.io/v3/protocols|jq -r '.[0].protocol_hash[0:12]')

LATEST_COMMIT=$(git ls-remote https://gitlab.com/tezos/tezos.git refs/heads/mainnet | cut -f 1)

# Ensure we don't have any file not found errors
touch ${OUTPUT}
touch ${NOTIFICATIONS_FILE}
touch ${PROTOCOLS_FILE}

curl -s "https://api6.tzscan.io/v3/bakings_endorsement/${BAKER}" | jq '.[]|{block, level}|select (.block==null)|.level' >> $OUTPUT

curl -s "https://api6.tzscan.io/v3/bakings/${BAKER}" | jq '.[]|{baked, level}|select (.baked == false)|.level' >> $OUTPUT

sort -u -o $OUTPUT $OUTPUT

# Missing baking/endorsements
while read p; do
    if ! grep -q "$p" $NOTIFICATIONS_FILE; then
        curl --header "Content-Type: application/json" \
            --request POST \
            --data "{\"service\":\"tezos\",\"status\":\"missed baking/endorsement for cycle $p\"}" \
            ${ZAPIER_URL}
        echo $p >> $NOTIFICATIONS_FILE
    fi
done <${OUTPUT}

# New Protocol
if ! grep -q "${CURRENT_PROTOCOL}" ${PROTOCOLS_FILE}; then
    curl --header "Content-Type: application/json" \
        --request POST \
        --data "{\"service\":\"tezos\",\"status\":\"New Protocol ${CURRENT_PROTOCOL}\"}" \
        ${ZAPIER_URL}
    echo ${CURRENT_PROTOCOL} >> ${PROTOCOLS_FILE}
fi

# New Commits to mainnet branch
if ! grep -q "${LATEST_COMMIT}" ${NOTIFICATIONS_FILE}; then
    curl --header "Content-Type: application/json" \
        --request POST \
        --data "{\"service\":\"tezos\",\"status\":\"New commits to mainnet ${LATEST_COMMIT}\"}" \
        ${ZAPIER_URL}
    echo ${LATEST_COMMIT} >> ${NOTIFICATIONS_FILE}
fi

declare -a services=("tezos.service" 
                "tezos-accuser.service"
                "tezos-baker.service"
                "tezos-cpr.service"
                "tezos-endorser.service"
                )

for service in "${services[@]}"
do
   if ! systemctl is-active ${service}; then
        curl --header "Content-Type: application/json" \
            --request POST \
            --data "{\"service\":\"tezos\",\"status\":\"Tezos service down ${service}\"}" \
            ${ZAPIER_URL}
    fi
done
